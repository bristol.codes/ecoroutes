import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
      title: '',
  },
  mutations: {
    CLEAR_TITLE: (state) => {
        state.title = '';
    },
    SET_TITLE: (state, title: string) => {
        state.title = title;
    },
  },
  actions: {

  },
});
